import { TypeJokesUseCase } from "../../src/usecases/type-jokes.usecase";


describe('Type joke use case', () =>{


    it('Should get programming type', async() =>{
        
        const programmingType = 'Programming';
        const search = '';
        const UseCase = new TypeJokesUseCase;
        const joke = await UseCase.execute(programmingType, search);

        joke.forEach(elem => {
            expect(elem.category).toBe('Programming')
        });
        
    })

    it('Should get Pun type', async() =>{
        
        const punType = 'Pun';
        const search = '';
        const UseCase = new TypeJokesUseCase;
        const joke = await UseCase.execute(punType, search);

        joke.forEach(elem => {
            expect(elem.category).toBe('Pun');
        });
        
    })

    it('Should get Misc type', async() =>{
        
        const miscType = 'Misc';
        const search = '';
        const UseCase = new TypeJokesUseCase;
        const joke = await UseCase.execute(miscType, search);

        joke.forEach(elem => {
            expect(elem.category).toBe('Misc');
        });
        
    })

    it('Should get Dark type', async() =>{
        
        const darkType = 'Dark';
        const search = '';
        const UseCase = new TypeJokesUseCase;
        const joke = await UseCase.execute(darkType, search);

        joke.forEach(elem => {
            expect(elem.category).toBe('Dark');
        });
        
    })
})