import { JokeRepository } from "../../src/repositories/jokes.repository";
import { AllJokesUseCase } from "../../src/usecases/all-jokes.usecase";
import  JOKES  from '../../fixtures/jokes.json';

jest.mock('../../src/repositories/jokes.repository');

describe('All jokes Use Case', () => {
    
    beforeEach(() => {
        JokeRepository.mockClear();
    })

    it('should execute', async () => {

        JokeRepository.mockImplementation(() =>{
            return {
                getAllJokes: () =>{
                    return JOKES;
                }
            }
        })
        
        const useCase = new AllJokesUseCase;
        const jokes = await useCase.execute();

        expect(jokes.length).toBe(4);

    })
})