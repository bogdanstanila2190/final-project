import { searchbar } from "../../src/services/search.service";

describe('Search bar service', () =>{

    it('Not found message', () =>{

        let inputValue;
        const inputResult = [{
            category: 'Not Found',
            joke: 'No jokes found'
        }]

        const search = searchbar(inputValue);

        expect(search).toStrictEqual(inputResult);

    });

    it('Return parameter value', () => {
        
        const inputValue = 'input value';
        const search = searchbar(inputValue);

        expect(search).toStrictEqual(inputValue);
    })
})