import './../components/joke.component';

export class JokePage extends HTMLElement{

    constructor() {
        super();
    }

    connectedCallback() {
        this.innerHTML = `
            <joke-component></joke-component>
        `;
    }
}

customElements.define('joke-page', JokePage);