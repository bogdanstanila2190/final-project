import { JokeRepository } from "../repositories/jokes.repository";

export class TypeJokesUseCase {
    
    async execute(jokeType, search) {
        const repository = new JokeRepository;
        const jokes = await repository.getTypeJokes(jokeType, search);
        return jokes.jokes;
    }
}