import { JokeRepository } from "../repositories/jokes.repository";

export class AllJokesUseCase {
    
    async execute() {
        const repository = new JokeRepository;
        const jokes = await repository.getAllJokes();
        return jokes.jokes;
    }
}