import { css, html, LitElement } from "lit";

export class HomeUI extends LitElement{


    static get styles() {
        return css`
        html, body, div, span, applet, object, iframe,
        h1, h2, h3, h4, h5, h6, p, blockquote, pre,
        a, abbr, acronym, address, big, cite, code,
        del, dfn, em, img, ins, kbd, q, s, samp,
        small, strike, strong, sub, sup, tt, var,
        b, u, i, center,
        dl, dt, dd, ol, ul, li,
        fieldset, form, label, legend,
        table, caption, tbody, tfoot, thead, tr, th, td,
        article, aside, canvas, details, embed, 
        figure, figcaption, footer, header, hgroup, 
        menu, nav, output, ruby, section, summary,
        time, mark, audio, video {
            margin: 0;
            padding: 0;
            border: 0;
            font-size: 100%;
            font: inherit;
            vertical-align: baseline;
        }
        a:link, a:visited, a:active {
            text-decoration:none;
        }
        .welcome {
            width: 95%;
            margin: 0 auto;
            font-size: 3rem;
            margin-bottom: 13rem;
            
        }

        #p-welcome{  
            color: var(--fifth-color);
        }

        #p-smile{
            color: var(--third-color);
        }

        #p-smile, #p-welcome{
            margin: .8rem;
            font-weight: 600;
        }

        nav {
            text-align: center
        }
        .jokeBtn{
            
            text-align: center;
            border: var(--main-color) solid .125rem;
            font-size: 2.5rem;
            padding: 2rem;
            border-radius: 1rem;
            color: var(--second-color);
            background-color: var(--third-color);
        }

        .jokeBtn:hover{
            border: var(--forth-color) solid .125rem;
            color: var(--forth-color);
        }
        .indexMain{
            margin: 5rem;
        }
        `;

    }

    render() {
        return html`
        <main class="indexMain">
            <section class="welcome">
                <p id="p-welcome">Welcome to the app</p>
                <p id="p-smile"> that will make you smile!</p>
            </section>
            <nav>
                <a href="/joke" class="jokeBtn" role="button" id="jokebtn">Tell me a Joke</a>
            </nav>
            
        </main>
        `;
    }
}

customElements.define('home-ui', HomeUI);