import {css, html, LitElement } from "lit";


export class JokeUI extends LitElement{

    static get styles() {
        return css`
            html, body, div, span, applet, object, iframe,
            h1, h2, h3, h4, h5, h6, p, blockquote, pre,
            a, abbr, acronym, address, big, cite, code,
            del, dfn, em, img, ins, kbd, q, s, samp,
            small, strike, strong, sub, sup, tt, var,
            b, u, i, center,
            dl, dt, dd, ol, ul, li,
            fieldset, form, label, legend,
            table, caption, tbody, tfoot, thead, tr, th, td,
            article, aside, canvas, details, embed, 
            figure, figcaption, footer, header, hgroup, 
            menu, nav, output, ruby, section, summary,
            time, mark, audio, video {
                margin: 0;
                padding: 0;
                border: 0;
                font-size: 100%;
                font: inherit;
                vertical-align: baseline;
}
            .card {
                width: 60%;
                border: var(--third-color) solid .125rem;
                border-radius: .5rem;
                padding: .8rem;
                margin: 1rem auto 1rem;
                background-color: var(--second-color);
            }
            .cardHeader {
                border-bottom: var(--main-color) solid .125rem;
                text-align: center;
            }
            .cardTitle{
                color: var(--fifth-color);
                font-size: 2rem;
                padding: 0.3rem;
            }
            .cardJoke{
                font-size: 1.4rem;
                padding: 0.3rem;
            }
        `;
    }
    

    static get properties() {
        return {
            jokes: {
                type: Array
            }
        }
    }


    render() {
        return html`
            ${this.jokes && this.jokes.map((joke) => html`
                <article class = "card" id = "card">

                    <header class = "cardHeader" id="cardHeader">
                            <h2 class = "cardTitle" id="cardtitle">${joke.category}</h2>
                    </header>
                    <h3 class = "cardJoke" id="cardJoke">${joke.joke}</h3>                

                </article>
            `)} 
        `;
    }

}

customElements.define('joke-ui', JokeUI);