import { Router } from '@vaadin/router';
import './pages/home.page';
import './pages/joke.page';


const outlet = document.querySelector('#outlet');
const router = new Router(outlet);

router.setRoutes([
    { path: '/', component: 'home-page' },
    { path: '/joke', component: 'joke-page' },
    { path: '(.*)', redirect: '/' }
]);