import { html, LitElement } from "lit";
import './../ui/home.ui';

export class HomeComponent extends LitElement{

    render(){
        return html `
            <home-ui></home-ui>
        `;
    };

}

customElements.define('home-component', HomeComponent);