import { css, html, LitElement } from "lit";
import { searchbar } from "../services/search.service";
import { AllJokesUseCase } from "../usecases/all-jokes.usecase";
import { TypeJokesUseCase } from "../usecases/type-jokes.usecase";
import './../ui/joke.ui';

export class JokeComponent extends LitElement{


    static get styles() {
        return css`

            .joke-container{
                display: flex;
                align-items: center;
                justify-content: center;
                flex-direction: column;
                margin-top: 4.5rem;
            }

            .buttonsDiv{
                width: 100%;
                text-align: center;
            }
            #jokeType, #searchInput{
                text-align: center;
                border-radius: .5rem;
                border: var(--second-color) solid .125rem;
                background-color:var(--third-color);
                font-size: 1.4rem;
                color: var(--second-color);
                padding: 1rem;
            }
            .cardBtn {
                border-radius: .5rem;
                border: var(--second-color) solid .125rem;
                background-color:var(--forth-color);
                color: var(--third-color);
                font-size: 1.4rem;
                padding: 1rem;
            }

            .cardBtn:hover{
                border: var(--forth-color) solid .125rem;
                color: var(--forth-color);
                background-color: var(--third-color);
            }
            
        
        `;

    }

    static get properties() {
        return {
            jokes: {
                type: Object,
                state: true
            }
        }
    }
    

    async onClick(){
        const jokeType = this.shadowRoot.querySelector('#jokeType').value;
        const search = this.shadowRoot.querySelector('#searchInput').value;

        const typeJokesUseCase = new TypeJokesUseCase();
        this.jokes = await typeJokesUseCase.execute(jokeType, search);
        this.jokes = searchbar(this.jokes);
    }
    
    async connectedCallback() {
        super.connectedCallback();
        const allJokesUseCase = new AllJokesUseCase();
        this.jokes = await allJokesUseCase.execute();
    }



    render(){
        return html `
            <joke-ui .jokes="${this.jokes}" class= "joke-container"></joke-ui>

            <div class="buttonsDiv" id="buttonsDiv">
                <input type="text" name="search" placeholder="Search" id="searchInput">
                <select name="select" id="jokeType">
                    <option value="Any" selected>Any</option>
                    <option value="Programming">Programming</option>
                    <option value="Miscellaneous">Misc</option>
                    <option value="Dark">Dark</option>
                    <option value="Pun">Pun</option>
                </select>
                <button class = "cardBtn" @click="${this.onClick}" id="cardBtn">Bring Joke</button>
            </div>
        `;
    }

   
}

customElements.define('joke-component', JokeComponent);