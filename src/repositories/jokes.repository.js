import axios from "axios";

export class JokeRepository {
    
    
    async getAllJokes() {
        return await (
            await axios.get('https://v2.jokeapi.dev/joke/Any?type=single&amount=4')
        ).data;
    }

    async getTypeJokes(jokeType, search) {
        return await (
            await axios.get(`https://v2.jokeapi.dev/joke/${jokeType}?type=single&contains=${search}&amount=4`)
        ).data;
    }

    
}