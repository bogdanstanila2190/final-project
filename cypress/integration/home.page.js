/// <reference types="Cypress" />

beforeEach(() => {

    cy.visit('/');
    cy.intercept('GET', 'https://v2.jokeapi.dev/joke/Any?type=single&amount=4', {fixture: './../../fixtures/jokes.json'});

  });

it('Go to joke page', () =>{

    cy.get('home-component').shadow().find('home-ui').shadow().find('.indexMain').find('nav').click();
    cy.get('joke-page').should('exist');
    
});