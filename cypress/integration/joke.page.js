/// <reference types="Cypress" />

beforeEach(() => {
    cy.visit('/joke');
    cy.intercept('GET', 'https://v2.jokeapi.dev/joke/Any?type=single&amount=4', {fixture: './../../fixtures/jokes.json'});

  });

it('Bring Programming category', () =>{
    
    cy.get('joke-component').shadow().find('#buttonsDiv').find('#jokeType').select('Programming').should('have.value', 'Programming');
    cy.get('joke-component').shadow().find('#buttonsDiv').find('#cardBtn').click();
    cy.get('joke-component').shadow().find('joke-ui').shadow().find('#card').find('#cardHeader').find('#cardtitle').should('contain', 'Programming');
})

it('Bring Misc category', () =>{

    cy.get('joke-component').shadow().find('#buttonsDiv').find('#jokeType').select('Misc').should('have.value', 'Miscellaneous');
    cy.get('joke-component').shadow().find('#buttonsDiv').find('#cardBtn').click();
    cy.get('joke-component').shadow().find('joke-ui').shadow().find('#card').find('#cardHeader').find('#cardtitle').should('contain', 'Misc');
})

it('Bring Dark category', () =>{

    cy.get('joke-component').shadow().find('#buttonsDiv').find('#jokeType').select('Dark').should('have.value', 'Dark');
    cy.get('joke-component').shadow().find('#buttonsDiv').find('#cardBtn').click();
    cy.get('joke-component').shadow().find('joke-ui').shadow().find('#card').find('#cardHeader').find('#cardtitle').should('contain', 'Dark');
})

it('Bring Pun category', () =>{

    cy.get('joke-component').shadow().find('#buttonsDiv').find('#jokeType').select('Pun').should('have.value', 'Pun');
    cy.get('joke-component').shadow().find('#buttonsDiv').find('#cardBtn').click();
    cy.get('joke-component').shadow().find('joke-ui').shadow().find('#card').find('#cardHeader').find('#cardtitle').should('contain', 'Pun');
})

it('Bring Not found  with search input', () => {

    cy.get('joke-component').shadow().find('#buttonsDiv').find('#searchInput').type('asdf');
    cy.get('joke-component').shadow().find('#buttonsDiv').find('#jokeType').select('Programming').should('have.value', 'Programming');
    cy.get('joke-component').shadow().find('#buttonsDiv').find('#cardBtn').click();
    cy.get('joke-component').shadow().find('joke-ui').shadow().find('#card').find('#cardHeader').find('#cardtitle').contains('Not Found');
})

it('Bring Programming category with search input', () => {

    cy.get('joke-component').shadow().find('#buttonsDiv').find('#searchInput').type('Programming');
    cy.get('joke-component').shadow().find('#buttonsDiv').find('#jokeType').select('Programming').should('have.value', 'Programming');
    cy.get('joke-component').shadow().find('#buttonsDiv').find('#cardBtn').click();
    cy.get('joke-component').shadow().find('joke-ui').shadow().find('#card').find('#cardJoke').contains('Programming');
})

it('Go back to home', () => {
    cy.get('#homejokeheader').click();
    cy.get('home-page').should('exist');
})