FROM node:14-alpine3.14 as builder
RUN mkdir /have-fun
WORKDIR /have-fun
COPY . .
RUN npm ci
RUN npm run build -- --output-path=dist

FROM nginx:1.21.6-alpine
COPY nginx/default.conf /etc/nginx/conf.d/
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /have-fun/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]